#!/usr/bin/nodejs

require("../../lib/easy-web-framework.js")();

option("hello","e","Give the hello messages.", "Do", "Hello", 1);
option("out","o","Stream the hello message to stdout", "View", false, 0);

start(function () {
    debug(options);
    info("Printing a hello message.");

    if(options["out"]) {
        console.log(options["hello"]);
    }
    return false;
});


